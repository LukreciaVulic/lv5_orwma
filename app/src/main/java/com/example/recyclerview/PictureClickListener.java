package com.example.recyclerview;

public interface PictureClickListener {
    void onPictureClick(int position);
}

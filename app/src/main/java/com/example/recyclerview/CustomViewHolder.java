package com.example.recyclerview;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private TextView tvIme;
    private ImageView ivIkona;
    private PictureClickListener clickListener;

    public CustomViewHolder(@NonNull View viewItem, PictureClickListener clickListener){
        super(viewItem);
        tvIme=(TextView)viewItem.findViewById(R.id.tvIme);
        ivIkona=(ImageView)viewItem.findViewById(R.id.ivIkona);
        this.clickListener=clickListener;
        ivIkona.setOnClickListener(this);

    }
    public void setName(String name){
        tvIme.setText(name);
    }

    @Override
    public void onClick(View v) {

        clickListener.onPictureClick(getAdapterPosition());
    }
}
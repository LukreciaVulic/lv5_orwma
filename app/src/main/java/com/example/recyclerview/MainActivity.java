package com.example.recyclerview;


import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements PictureClickListener{

    private RecyclerView recycler;
    private RecyclerAdapter adapter;
    private EditText etIme;
    private Button btnDodaj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        InitializeId();
        setupRecycler();
        setupRecyclerData();
    }

    private void InitializeId(){
        etIme=(EditText) findViewById(R.id.etIme);
        btnDodaj=(Button)findViewById(R.id.btnDodaj);
    }

    private void setupRecycler(){
        recycler=(RecyclerView)findViewById(R.id.recyclerView);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        adapter=new RecyclerAdapter(this);
        recycler.setAdapter(adapter);
    }

    private void setupRecyclerData(){
        List<String> data= new ArrayList<>();
        adapter.addData(data);
    }

    public void OnClickAddItem(View view){
        String input=etIme.getText().toString();
        if(input.matches("")){
            Toast.makeText(this, "Unesite ime", Toast.LENGTH_LONG).show();
            return;
        }
        else adapter.addNewItem(input);

    }



    @Override
    public void onPictureClick(int position) {
        adapter.removeItem(position);
    }
}
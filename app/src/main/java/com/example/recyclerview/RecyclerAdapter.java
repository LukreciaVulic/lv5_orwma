package com.example.recyclerview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<CustomViewHolder> {

    private List<String> dataList = new ArrayList<>();
    public static int count=0;
    private PictureClickListener clickListener;

    public RecyclerAdapter(PictureClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View listItem= LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.list_item,parent,false);
        return new CustomViewHolder(listItem,clickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        holder.setName(dataList.get(position));
    }


    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public void addData(List<String> data){
        dataList.clear();
        dataList.addAll(data);
        notifyDataSetChanged();
    }

    public void addNewItem(String data){
        dataList.add(data);
        notifyItemChanged(count);
        count=count+1;
    }

    public void removeItem(int position){
        dataList.remove(position);
        notifyItemChanged(count);
        count=count-1;
    }


}
